#!/bin/bash

set -e

/etc/init.d/mysql start
while !(mysqladmin ping); do
      sleep .5
done

exec "$@"
